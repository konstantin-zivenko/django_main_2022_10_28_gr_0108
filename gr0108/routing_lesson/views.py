from django.http import HttpRequest, HttpResponse

def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Rout 1 is here")

def special_case_2003(request: HttpRequest) -> HttpResponse:
    return HttpResponse("спеціальний випадок 2003")

def year_archive(request: HttpRequest, year: int | None) -> HttpResponse:
    return HttpResponse(f"архів за рік {year}")

def month_archive(request: HttpRequest, year: int | None, month: int | None) -> HttpResponse:
    return HttpResponse(f"архів за місяць {month} - {year}")

def article_detail(
        request: HttpRequest,
        year: int | None,
        month: int | None,
        slug: str | None,
) -> HttpResponse:
    return HttpResponse(f"деталізація статті {slug} за {month} - {year}")



# Create your views here.
