# django_main_2022_10_28_gr_0108

---

[video - intro](https://youtu.be/70VDsjdq2ng)

[video 2 - routing](https://youtu.be/sUdkjssw-6k)

[video 3 - models](https://youtu.be/K0Xe-rqjsuY)

[video 4 - admin&views](https://youtu.be/SdfTesm8rdQ)

[video 5 - templates&forms](https://youtu.be/rXQoo9ZL6gY)

[video 6 - forms&generic obj](https://youtu.be/M7F66_Ctf-s)

[video 7 - generic_and_DRF_REST](https://youtu.be/68WUh4O-hUA)

[video 8 - auth](https://youtu.be/7vN4wbn9nDY)

[video 9 - auth 2](https://youtu.be/JlAnOuaIskk)

[video 10 - deployment](https://youtu.be/stglzpA9S7A)
